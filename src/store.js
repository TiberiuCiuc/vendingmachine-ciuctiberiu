import {createStore, combineReducers} from 'redux';
import moneyReducer from './reducers/moneyReducers';
import productReducer from './reducers/productReducers';

const rootReducer = combineReducers({
  moneyReducer: moneyReducer,
  productReducer: productReducer,
});

const configStore = () => createStore(rootReducer);

export default configStore;
