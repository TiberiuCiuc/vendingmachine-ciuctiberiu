import React, {Component} from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Dashboard from './src/components/TabNavigator';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <StatusBar barStyle="light-content" animated={true} />
        <NavigationContainer>
          <Dashboard />
        </NavigationContainer>
      </React.Fragment>
    );
  }
}
