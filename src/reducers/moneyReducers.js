import {ADD_MONEY, SUBTRACT_MONEY} from '../actions/types';

const initialState = {
  money: 30,
};

const moneyReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_MONEY:
      let moneyNumber = parseInt(action.data);
      if (moneyNumber) {
        return {
          ...state,
          money: state.money + action.data,
        };
      } else {
        return state;
      }

    case SUBTRACT_MONEY:
      return {
        ...state,
        money: state.money - action.data,
      };
    default:
      return state;
  }
};

export default moneyReducer;
