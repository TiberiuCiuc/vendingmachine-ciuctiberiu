import {ADD_MONEY, SUBTRACT_MONEY} from './types';

export const addMoney = (money) => ({
  type: ADD_MONEY,
  data: money,
});

export const subtractMoney = (money) => ({
  type: SUBTRACT_MONEY,
  data: money,
});
