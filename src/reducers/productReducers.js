import {SUBTRACT_PRODUCT} from '../actions/types';

const initialState = {
  items: [
    {
      id: 1,
      name: 'Snikers',
      price: 12,
      quantity: 36,
      image: require('../images/Snikers.jpeg'),
    },
    {
      id: 2,
      name: 'Twix',
      price: 10,
      quantity: 30,
      image: require('../images/Twix.jpeg'),
    },
    {
      id: 3,
      name: 'Lion',
      price: 15,
      quantity: 32,
      image: require('../images/Lion.jpeg'),
    },
    {
      id: 4,
      name: 'Lipton',
      price: 18,
      quantity: 10,
      image: require('../images/Lipton.jpeg'),
    },
    {
      id: 5,
      name: 'Lays',
      price: 5,
      quantity: 20,
      image: require('../images/Lays.jpeg'),
    },
    {
      id: 6,
      name: 'Chio',
      price: 6,
      quantity: 15,
      image: require('../images/Chio.jpeg'),
    },
    {
      id: 7,
      name: '7 Days',
      price: 3,
      quantity: 20,
      image: require('../images/7Days.jpeg'),
    },
    {
      id: 8,
      name: 'Heidi',
      price: 7,
      quantity: 50,
      image: require('../images/Heidi.jpeg'),
    },
    {
      id: 9,
      name: 'Water',
      price: 4,
      quantity: 12,
      image: require('../images/Water.jpeg'),
    },
  ],
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SUBTRACT_PRODUCT:
      let updatedItems = [...state.items];
      let item = state.items.filter((item) => item.id == action.id)[0];
      let index = state.items.indexOf(item);
      updatedItems[index] = item;
      item.quantity -= 1;
      return {
        ...state,
        items: updatedItems,
      };
    default:
      return state;
  }
};

export default productsReducer;
