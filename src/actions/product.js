import {SUBTRACT_PRODUCT} from './types';

export const subtractProduct = (id) => ({
  type: SUBTRACT_PRODUCT,
  id: id,
});
