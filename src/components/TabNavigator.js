import React, {Component} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
// import Options from '../screens/Options';
import VendingMachine from '../screens/VendingMachine';
import Goods from '../screens/Goods';

const Tab = createMaterialTopTabNavigator();

export default class TabNavigator extends Component {
  static navigationOptions = {
    title: 'DB',
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Tab.Navigator initialRouteName="Dashboard">
        <Tab.Screen name="Dashboard" component={VendingMachine} />
      </Tab.Navigator>
    );
  }
}
