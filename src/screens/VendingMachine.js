import React, {Component} from 'react';

import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Modal,
} from 'react-native';

import {connect} from 'react-redux';
import {addMoney, subtractMoney} from '../actions/money';
import {subtractProduct} from '../actions/product';

class VendingMachine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      noMoneyModal: false,
      addMoney: '',
      codeNumber: 0,
      walletModal: false,
      inspectingProductModal: false,
      inspectingProduct: null,
      enteredProductCode: '',
      selectingProduct: null,
      selectedProduct: null,
      inputPanelButtons: 11,
      inputPanelButtonsArray: [
        {
          display: '1',
          value: 1,
        },
        {
          display: '2',
          value: 2,
        },
        {
          display: '3',
          value: 3,
        },
        {
          display: '4',
          value: 4,
        },
        {
          display: '5',
          value: 5,
        },
        {
          display: '6',
          value: 6,
        },
        {
          display: '7',
          value: 7,
        },
        {
          display: '8',
          value: 8,
        },
        {
          display: '9',
          value: 9,
        },
        {
          display: 'C',
          value: 10,
        },
        {
          display: '0',
          value: 0,
        },
        {
          display: 'OK',
          value: 11,
        },
      ],
      inputPanelModal: false,
      // items: [
      //   {
      //     id: 1,
      //     name: 'Snikers',
      //     price: 12,
      //     quantity: 36,
      //     image: require('../images/Snikers.jpeg'),
      //   },
      //   {
      //     id: 2,
      //     name: 'Twix',
      //     price: 10,
      //     quantity: 30,
      //     image: require('../images/Twix.jpeg'),
      //   },
      //   {
      //     id: 3,
      //     name: 'Lion',
      //     price: 15,
      //     quantity: 32,
      //     image: require('../images/Lion.jpeg'),
      //   },
      //   {
      //     id: 4,
      //     name: 'Lipton',
      //     price: 18,
      //     quantity: 10,
      //     image: require('../images/Lipton.jpeg'),
      //   },
      //   {
      //     id: 5,
      //     name: 'Lays',
      //     price: 5,
      //     quantity: 20,
      //     image: require('../images/Lays.jpeg'),
      //   },
      //   {
      //     id: 6,
      //     name: 'Chio',
      //     price: 6,
      //     quantity: 15,
      //     image: require('../images/Chio.jpeg'),
      //   },
      //   {
      //     id: 7,
      //     name: '7 Days',
      //     price: 3,
      //     quantity: 20,
      //     image: require('../images/7Days.jpeg'),
      //   },
      //   {
      //     id: 8,
      //     name: 'Heidi',
      //     price: 7,
      //     quantity: 50,
      //     image: require('../images/Heidi.jpeg'),
      //   },
      //   {
      //     id: 9,
      //     name: 'Water',
      //     price: 4,
      //     quantity: 12,
      //     image: require('../images/Water.jpeg'),
      //   },
      // ],
    };
  }

  inputPanel(size = 'small') {
    return (
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          paddingTop: 10,
          paddingBottom: 10,
        }}>
        <View
          style={{
            display: 'flex',
            width: '90%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(220,130,100,1)',
            marginBottom: 5,
          }}>
          <Text
            style={{fontSize: size === 'small' ? 8 : 16, fontWeight: 'bold'}}>
            Enter product code
          </Text>
        </View>
        <View
          style={{
            display: 'flex',
            width: '90%',
            height: 60,
            marginBottom: 10,
            backgroundColor: 'rgba(0, 0, 0, 1)',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TextInput
            style={{
              display: 'flex',
              width: '90%',
              color: 'rgba(0, 0, 0, 1)',
              backgroundColor: 'rgba(200, 200, 255, 1)',
              borderColor: 'rgba(0, 255, 0, 1)',
              borderWidth: 1,
            }}
            textAlign="right"
            editable={false}
            onChangeText={(text) => {
              this.setState({enteredProductCode: text});
            }}
            value={this.state.enteredProductCode}
            placeholder={'0000'}
          />
        </View>
        {this.state.inputPanelButtonsArray.map((button) =>
          this.renderButton(button, size),
        )}
      </View>
    );
  }

  createVendingInputPanel(isItmodal) {
    return (
      <React.Fragment>
        {isItmodal ? (
          <View>{this.inputPanel('big')}</View>
        ) : (
          <React.Fragment>
            {/* Input Panel */}
            <TouchableOpacity
              onPress={() => {
                this.setInputPanelModal(!this.state.inputPanelModal);
              }}>
              {this.inputPanel()}
            </TouchableOpacity>
            {/* Buy Button */}
            <TouchableOpacity
              style={{
                width: '90%',
                marginTop: '50%',
                backgroundColor: 'rgba(255, 255, 255, 1)',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
              }}
              onPress={() => {
                this.buyProduct(this.state.selectedProduct);
              }}>
              <Text style={{fontWeight: '600', fontSize: 16}}>Buy</Text>
            </TouchableOpacity>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }

  selectProduct(code, append) {
    if (append) {
      let productCode = this.state.enteredProductCode + code;
      let codeNumber = parseInt(productCode);
      let product = this.props.items.filter((P) => P.id == codeNumber)[0];
      if (product) {
        this.setState({
          selectingProduct: product,
          enteredProductCode: productCode,
        });
      } else {
        this.setState({
          selectingProduct: null,
          enteredProductCode: productCode,
        });
      }
    } else {
      this.setState({
        selectedProduct: null,
        selectingProduct: null,
        enteredProductCode: '',
      });
    }
  }

  renderButton(button, size) {
    return (
      <TouchableOpacity
        key={button.value}
        style={{
          display: 'flex',
          width: '28%',
          height: size === 'small' ? '10%' : '15%',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'rgba(180,180,255,1)',
          borderRadius: 2,
          margin: 1,
        }}
        onPress={() => {
          if (button.value < 10) {
            this.selectProduct(button.display, true);
          } else if (button.value == 10) {
            this.selectProduct(null, false);
          } else {
            this.setState({
              selectedProduct: this.state.selectingProduct,
              selectingProduct: null,
              inputPanelModal: false,
            });
          }
        }}>
        <Text style={{fontSize: 12}}>{button.display}</Text>
      </TouchableOpacity>
    );
  }

  renderItem(item, index) {
    return (
      <TouchableOpacity
        key={index}
        style={{
          display: 'flex',
          width: '30%',
          height: '30%',
          backgroundColor:
            this.state.selectedProduct != null &&
            item.id === this.state.selectedProduct.id
              ? 'rgba(0, 255, 0, 0.5)'
              : this.state.selectingProduct != null &&
                item.id === this.state.selectingProduct.id
              ? 'rgba(0, 0, 255, 0.5)'
              : 'rgba(15,125,105,0.8)',
          borderRadius: 10,
          paddingTop: 5,
          paddingBottom: 5,
          margin: 5,
        }}
        onPress={() => {
          this.setProductModal(item, !this.state.inspectingProductModal);
        }}>
        <View
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{color: 'rgba(255,255,255,1)', fontSize: 14}}>
            {`${item.name} : ${item.price} $`}
          </Text>
          <Image
            style={{
              display: 'flex',
              flex: 2,
              width: '70%',
              borderRadius: 20,
            }}
            source={item.image}
          />
          <Text style={{color: 'rgba(255,255,255,1)', fontSize: 10}}>
            Remaining: {item.quantity}
          </Text>
          <Text style={{color: 'rgba(255,255,255,1)', fontSize: 12}}>
            Code: {item.id}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  setProductModal(product, visible) {
    this.setState({
      inspectingProduct: product,
      inspectingProductModal: visible,
    });
  }

  setInputPanelModal(visible) {
    this.setState({selectingProduct: null, inputPanelModal: visible});
  }

  showWalletModal(visible) {
    this.setState({walletModal: visible});
  }

  buyProduct(product) {
    if (product) {
      if (this.props.money < product.price) {
        this.setState({noMoneyModal: !this.state.noMoneyModal});
      } else {
        this.props.subtractM(product.price);
        this.props.subtractP(product.id);
        this.setState({
          selectedProduct: null,
          selectingProduct: null,
          enteredProductCode: '',
        });
      }
    } else {
      console.log('nothing');
    }
  }

  render() {
    return (
      <View
        style={{
          display: 'flex',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: 10,
          paddingBottom: 10,
        }}>
        {/* Modals */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={
            this.state.walletModal ||
            this.state.inspectingProductModal ||
            this.state.inputPanelModal ||
            this.state.noMoneyModal
          }>
          <View
            style={{
              display: 'flex',
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'rgba(200, 200, 200, 0.6)',
            }}>
            {/* No Money Modal */}
            {this.state.noMoneyModal ? (
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '80%',
                  height: '30%',
                  backgroundColor: 'rgba(255, 255, 255, 1)',
                  borderRadius: 20,
                  padding: 10,
                }}>
                <Text>
                  You don't have enough money ! Add more money to the wallet !
                </Text>
                <TouchableOpacity
                  style={{
                    width: '30%',
                    height: '15%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor: 'rgba(0, 0, 0, 1)',
                    borderWidth: 1,
                    borderRadius: 5,
                    margin: 5,
                  }}
                  onPress={() => {
                    this.setState({noMoneyModal: !this.state.noMoneyModal});
                  }}>
                  <Text>Ok</Text>
                </TouchableOpacity>
              </View>
            ) : null}
            {/* Wallet Modal */}
            {this.state.walletModal ? (
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  width: '80%',
                  height: '40%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'rgba(50, 50, 50, 1)',
                  borderRadius: 50,
                  paddingTop: 20,
                  paddingBottom: 20,
                }}>
                <View
                  style={{
                    display: 'flex',
                    flex: 2,
                    width: '80%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                  }}>
                  <TextInput
                    style={{
                      width: '50%',
                      backgroundColor: 'rgba(255, 255, 255, 1)',
                      borderRadius: 20,
                    }}
                    keyboardType="numeric"
                    onChangeText={(text) => {
                      this.setState({addMoney: text});
                    }}
                    value={this.state.addMoney}
                    placeholder={'0'}
                    textAlign="right"
                  />
                  <TouchableOpacity
                    style={{
                      width: '20%',
                      height: '20%',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: 'rgba(0, 255, 0, 1)',
                      borderRadius: 20,
                      borderWidth: 1,
                      borderColor: 'rgba(255, 255, 255, 1)',
                    }}
                    onPress={() => {
                      this.props.addM(parseInt(this.state.addMoney));
                      this.setState({
                        addMoney: '',
                        walletModal: !this.state.walletModal,
                      });
                    }}>
                    <Text>Add</Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    display: 'flex',
                    flex: 1,
                    width: '80%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                  }}>
                  <TouchableOpacity
                    style={{
                      width: '20%',
                      height: '40%',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: 'rgba(255, 0, 0, 1)',
                      borderRadius: 20,
                      borderWidth: 1,
                      borderColor: 'rgba(255, 255, 255, 1)',
                    }}
                    onPress={() => {
                      this.showWalletModal(!this.state.walletModal);
                    }}>
                    <Text>Close</Text>
                  </TouchableOpacity>
                </View>
              </View>
            ) : null}

            {/* Inspecting Product Modal */}
            {this.state.inspectingProductModal &&
            this.state.inspectingProduct != null ? (
              <View
                style={{
                  display: 'flex',
                  width: '85%',
                  height: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'rgba(15,125,105,1)',
                  borderRadius: 50,
                }}>
                <Text style={{color: 'rgba(255,255,255,1)', fontSize: 14}}>
                  {`${this.state.inspectingProduct.name} : ${this.state.inspectingProduct.price} $`}
                </Text>
                <Image
                  style={{
                    width: '70%',
                    height: '50%',
                    borderRadius: 20,
                    margin: 10,
                  }}
                  source={this.state.inspectingProduct.image}
                />
                <Text style={{color: 'rgba(255,255,255,1)', fontSize: 10}}>
                  Remaining: {this.state.inspectingProduct.quantity}
                </Text>
                <Text style={{color: 'rgba(255,255,255,1)', fontSize: 12}}>
                  Code: {this.state.inspectingProduct.id}
                </Text>
                <TouchableOpacity
                  style={{
                    width: '25%',
                    height: '8%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderWidth: 1,
                    borderColor: 'rgba(255, 255, 255, 1)',
                    borderRadius: 10,
                    margin: 10,
                  }}
                  onPress={() => {
                    this.setProductModal(
                      null,
                      !this.state.inspectingProductModal,
                    );
                  }}>
                  <Text style={{color: 'rgba(255,255,255,1)', fontSize: 12}}>
                    OK
                  </Text>
                </TouchableOpacity>
              </View>
            ) : null}

            {/*  Input Product Panel Modal */}
            {this.state.inputPanelModal ? (
              <View
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                  width: '80%',
                  height: '80%',
                  paddingTop: 20,
                  paddingBottom: 20,
                  borderRadius: 50,
                  backgroundColor: 'rgba(50, 50, 50, 1)',
                }}>
                {/* Input Panel */}
                <View
                  style={{
                    display: 'flex',
                    width: '90%',
                    height: '45%',
                  }}>
                  {this.createVendingInputPanel(true)}
                </View>
                <View
                  style={{
                    display: 'flex',
                    width: '90%',
                    height: '45%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 5,
                  }}>
                  <Text style={{color: 'rgba(255, 255, 255, 1)', fontSize: 18}}>
                    Product Preview
                  </Text>
                  {this.state.selectingProduct != null ? (
                    <View
                      style={{
                        display: 'flex',
                        flex: 1,
                        width: '90%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(15,125,105,1)',
                        borderRadius: 50,
                      }}>
                      <Text
                        style={{color: 'rgba(255,255,255,1)', fontSize: 14}}>
                        {`${this.state.selectingProduct.name} : ${this.state.selectingProduct.price} $`}
                      </Text>
                      <Image
                        style={{
                          width: '70%',
                          height: '50%',
                          borderRadius: 20,
                          margin: 10,
                        }}
                        source={this.state.selectingProduct.image}
                      />
                      <Text
                        style={{color: 'rgba(255,255,255,1)', fontSize: 10}}>
                        Remaining: {this.state.selectingProduct.quantity}
                      </Text>
                      <Text
                        style={{color: 'rgba(255,255,255,1)', fontSize: 12}}>
                        Code: {this.state.selectingProduct.id}
                      </Text>
                    </View>
                  ) : (
                    <View
                      style={{
                        display: 'flex',
                        flex: 1,
                        width: '90%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(15,125,105,1)',
                        borderRadius: 50,
                      }}>
                      <Text
                        style={{color: 'rgba(255, 255, 255, 1)', fontSize: 18}}>
                        NONE
                      </Text>
                    </View>
                  )}
                </View>
                <TouchableOpacity
                  onPress={() => {
                    this.setInputPanelModal(!this.state.inputPanelModal);
                  }}>
                  <Text>Close</Text>
                </TouchableOpacity>
              </View>
            ) : null}
          </View>
        </Modal>
        {/* Vending Machine  */}
        <View
          style={{
            display: 'flex',
            flex: 1,
            width: '95%',
            flexDirection: 'row',
            marginTop: '5%',
            borderWidth: 3,
            borderColor: 'rgba(0,0,0,1)',
            backgroundColor: 'rgba(0,0,0,1)',
          }}>
          {/* Vending Machine Left Panel */}
          <View
            style={{
              display: 'flex',
              flex: 5,
              backgroundColor: 'rgba(200, 200, 200, 0.2)',
            }}>
            {/* Wallet */}
            <View
              style={{
                display: 'flex',
                flex: 1,
                flexDirection: 'row',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}>
              <View
                style={{
                  width: '60%',
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: 20, color: '#FFFFFF'}}>
                  {`Wallet money: ${this.props.money} $`}
                </Text>
              </View>

              <TouchableOpacity
                style={{
                  width: '30%',
                  height: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderColor: 'white',
                  borderWidth: 1,
                  borderRadius: 20,
                  backgroundColor: 'green',
                }}
                onPress={() => {
                  this.showWalletModal(!this.state.walletModal);
                }}>
                <Text>Add Money</Text>
              </TouchableOpacity>
            </View>
            {/* Display */}
            <View
              style={{
                display: 'flex',
                flex: 6,
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                backgroundColor: 'rgba(200, 200, 200, 0.1)',
                borderTopWidth: 2,
                borderBottomWidth: 2,
                borderColor: 'rgba(50, 75, 120, 1)',
                paddingTop: 10,
                paddingBottom: 10,
              }}>
              {this.props.items.map((item) => this.renderItem(item, item.id))}
            </View>
            {/* Product */}
            <View
              style={{
                display: 'flex',
                flex: 2,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '',
              }}>
              <Text>Product</Text>
            </View>
          </View>

          {/* Vending Machine Input Panel */}
          <View
            style={{
              display: 'flex',
              flex: 1,
              flexDirection: 'column',
              borderLeftWidth: 3,
              borderColor: 'rgba(0,0,0,1)',
              backgroundColor: 'rgba(200,200,200,0.2)',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {this.createVendingInputPanel(false)}
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    money: state.moneyReducer.money,
    items: state.productReducer.items,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addM: (money) => dispatch(addMoney(money)),
    subtractM: (money) => dispatch(subtractMoney(money)),
    subtractP: (id) => dispatch(subtractProduct(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(VendingMachine);
